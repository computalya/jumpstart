# template.rb
require 'fileutils'
require 'shellwords'

@current_dir = "#{File.expand_path(File.dirname(__FILE__))}"

# functions

def add_template_repository_to_source_path
  # Copied from: https://github.com/mattbrictson/rails-template
  # Add this template directory to source_paths so that Thor actions like
  # copy_file and template resolve against our source files. If this file was
  # invoked remotely via HTTP, that means the files are not present locally.
  # In that case, use `git clone` to download them to a local temporary dir.

  if __FILE__ =~ %r{\Ahttps?://}
    require 'tmpdir'
    source_paths.unshift(tempdir = Dir.mktmpdir('jumpstart-'))
    at_exit { FileUtils.remove_entry(tempdir) }
    git clone: [
      '--quiet',
      'https://gitlab.com/computalya/jumpstart.git',
      tempdir
    ].map(&:shellescape).join(" ")

    if (branch = __FILE__[%r{jumpstart/(.+)/template.rb}, 1])
      Dir.chdir(tempdir) { git checkout: branch }
    end
  else
    source_paths.unshift(File.dirname(__FILE__))
  end
end

def add_gems
  # gem_group :development, :test do
    # gem 'rspec-rails'
    # gem 'capybara'
  # end

  gem_group :development do
    gem 'faker'
  end

  gem_group :test do
    gem 'factory_bot_rails'
    gem 'database_cleaner'
  end

  gem 'devise'
  gem 'rolify'
  gem 'pundit'
  gem 'hirb'
  gem 'colorize'
  gem 'rubocop-rails_config'
end

def add_rubocop
  #
  generate 'rubocop_rails_config:install'
end

def add_jquery
  run 'yarn add jquery'

  insert_into_file( 'app/javascript/packs/application.js', "\nrequire(\"jquery\")", after: "require(\"turbolinks\").start()" )

  webpack = "const webpack = require(\"webpack\")\n" \
            "environment.plugins.append(\"Provide\", new webpack.ProvidePlugin(\{\n" \
            "  \$: 'jquery',\n" \
            "  jQuery: 'jquery'\n" \
            "\}))\n\n"

  insert_into_file( 'config/webpack/environment.js', webpack, before: 'module.exports = environment' )
end

def add_hirb
  inject_into_file 'config/application.rb', before: /^  end\n/ do <<-RUBY
    console do
      # Action(s) I want to run on console load...
      Hirb.enable
    end
    RUBY
  end

end

def set_application_name
  # Add Application Name to Config
  environment "config.application_name = Rails.application.class.module_parent_name"

  # Announce the user where he can change the application name in the future.
  puts "You can change application name inside: ./config/application.rb"
end

def add_foundation
  # add custom css
  # append_file 'app/assets/stylesheets/foundation_and_overrides.scss',
  #     "\n.callout.notice{\n  // usage: class: \"callout notice\"\n  @extend .success;\n}"
  Dir.mkdir 'app/javascript/src'
  run 'yarn add foundation-sites motion-ui foundation-icons'

  scss = "\@import '~foundation-sites/scss/foundation';\n" \
         "\@include foundation-everything;\n" \
         "\@import '~motion-ui/motion-ui';"
  create_file 'app/javascript/src/application.scss', scss

  packs = "\nimport \"foundation-sites\"\n" \
          "import \"foundation-icons/foundation-icons.scss\"\n" \
          "require(\"src/application\")\n\n" \
          "\$(document).on('turbolinks:load', function() \{\n" \
          "  \$(document).foundation()\n" \
          "\});"
  append_file 'app/javascript/packs/application.js', packs

  style = "\n    <\%= stylesheet_pack_tag 'application', media: 'all', 'data-turbolinks-track': 'reload' \%>"
  insert_into_file( 'app/views/layouts/application.html.erb', style,
                    after: "\n    <\%= stylesheet_link_tag 'application', media: 'all', 'data-turbolinks-track': 'reload' \%>" )
end

def copy_templates
  directory "app", force: true

  route "get 'secure_pages/user_dashboard'"
  route "get 'secure_pages/admin_dashboard'"
  route "get 'secure_pages/common_dashboard'"

  route "get 'home/index'"
  route "root 'home#index'"
end

def stop_spring
  #
  run "pkill spring"
end

def add_rspec
  generate 'rspec:install'
  Dir.mkdir 'spec/factories'

  append_file ".rspec", "--require rails_helper\n"

  # add configuration for rspec and capybara
  inject_into_file("spec/rails_helper.rb",
          "  config.include Capybara::DSL\n  config.include FactoryBot::Syntax::Methods\n",
          before: /^end\n/)

  gsub_file 'spec/rails_helper.rb',
                            "# Dir[Rails.root.join('spec', 'support', '**', '*.rb')].each { |f| require f }",
                            "Dir[Rails.root.join('spec', 'support', '**', '*.rb')].each { |f| require f }"
end

def add_devise
  stop_spring
  generate 'devise:install'

  stop_spring
  generate 'devise:views'

  stop_spring
  generate 'devise User'

  # enable trackable
  # get file name of the last migration
  migration = Dir.glob("db/migrate/*").max_by{ |f| File.mtime(f) }

  gsub_file migration,  '# t.integer  :sign_in_count, default: 0, null: false',
                        't.integer  :sign_in_count, default: 0, null: false'

  gsub_file migration,  '# t.datetime :current_sign_in_at',
                        't.datetime :current_sign_in_at'

  gsub_file migration,  '# t.datetime :last_sign_in_at',
                        't.datetime :last_sign_in_at'

  gsub_file migration,  '# t.string   :current_sign_in_ip',
                        't.string     :current_sign_in_ip'

  gsub_file migration,  '# t.string   :last_sign_in_ip',
                        't.string     :last_sign_in_ip'

  # Configure Devise
  environment "config.action_mailer.default_url_options = { host: 'localhost', port: 3000 }",
              env: 'development'
end

def add_rolify
  stop_spring
  generate 'rolify Role User'

  gsub_file 'config/initializers/rolify.rb',
                            '# config.use_dynamic_shortcuts',
                            'config.use_dynamic_shortcuts'

  # copy files
  directory "lib", force: true
  directory "spec", force: true
  
  # bug fix for rails 6.0
  migration = Dir.glob("db/migrate/*").max_by{ |f| File.mtime(f) }
  gsub_file migration,  'class RolifyCreateRoles < ActiveRecord::Migration',
                        'class RolifyCreateRoles < ActiveRecord::Migration[6.0]'
end

def add_pundit
  # stop_spring
  # generate 'pundit:install'
  Dir.mkdir "app/policies"
  copy_file "#{@current_dir}/application/pundit/app/policies/application_policy.rb", "app/policies"
end

def add_users
  directory 'db', force: true
  rails_command 'db:seed'
end

def update_gitignore
  #
  append_file ".gitignore", "\n.DS_Store"
end

# main
add_template_repository_to_source_path

add_gems

after_bundle do
  set_application_name
  add_hirb

  stop_spring
  rails_command 'db:create'

  add_rubocop
  # add_rspec
  add_jquery                              # foundation needs jquery
  add_foundation

  add_devise
  add_rolify
  add_pundit

  copy_templates
  rails_command 'db:migrate'
  add_users

  update_gitignore
  git :init
  git add: "."
  git commit: %Q{ -m 'Initial commit' }
end
