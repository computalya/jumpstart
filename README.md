# Rails Application Templates

## To Do

- [ ] update this README
- [ ] add a README for the app created with jumpstart
- [ ] add ActiveStorage

## Description

easy jump start application template for Rails 6.0

## Getting Started

#### demoapp using Postgresql

```bash
rails new demoapp -d postgresql -m https://gitlab.com/computalya/jumpstart/raw/master/template.rb
```

Or, using locale copy:

```bash
rails new demoapp -d postgresql -m template.rb
```

Or, using a environment path

```bash
export template="path_to/template.rb"

rails new demoapp -m $template
```

#### Cleaning up

```bash
rails db:drop
cd ..
rm -rf demoapp
```
