user  = User.create! email: 'user@example.com',  password: 'password', password_confirmation: 'password'
user.add_role :user

admin  = User.create! email: 'admin@example.com',  password: 'password', password_confirmation: 'password'
admin.remove_role :user
admin.add_role :admin
