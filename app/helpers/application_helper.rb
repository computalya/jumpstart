module ApplicationHelper
  def get_flash_type
   return 'alert'  unless flash.alert.nil?
   return 'notice' unless flash.notice.nil?
  end

  def flash_message
    content_tag(:div, class: "\#{get_flash_type} callout text-center") do
      flash.send(get_flash_type)
    end
  end
end  