class SecurePagePolicy < Struct.new(:user, :secure_page)
  attr_reader :current_user, :model

  def initialize(current_user, model)
    @current_user = current_user
    @user = model
  end

  def user_dashboard?
    @current_user.has_role? :user
  end

  def admin_dashboard?
    @current_user.has_role? :admin
  end

  def common_dashboard?
    (@current_user.has_role? :admin) || (@current_user.has_role? :user)
  end  
end
